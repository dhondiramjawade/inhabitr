import { NgModule } from '@angular/core';
import { QuoteListComponent } from './components/quote-list/quote-list.component';
import { QuoteComponent } from './components/quote/quote.component';



@NgModule({
  declarations: [
  
    QuoteListComponent,
       QuoteComponent
  ],
  imports: [
  ],
  exports: [
  ]
})
export class QuoteModule { }
